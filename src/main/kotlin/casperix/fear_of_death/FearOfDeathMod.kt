package casperix.fear_of_death

import casperix.fear_of_death.events.KillEvent
import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.entity.event.v1.ServerLivingEntityEvents
import org.slf4j.LoggerFactory

@Suppress("unused")
object FearOfDeathMod : ModInitializer {
    val logger = LoggerFactory.getLogger("fear-of-death")

	override fun onInitialize() {
		ServerLivingEntityEvents.ALLOW_DEATH.register(KillEvent)

		logger.info("Initialized")
	}
}

