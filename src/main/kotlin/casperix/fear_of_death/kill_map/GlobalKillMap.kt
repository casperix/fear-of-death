package casperix.fear_of_death.kill_map

import net.minecraft.entity.EntityType
import net.minecraft.util.math.Vec3d
import net.minecraft.util.math.Vec3i

class GlobalKillMap {
	internal val data = mutableMapOf<Vec3i, RegionKillMap>()

	fun getKillInfo(position: Vec3i, entityType: EntityType<*>): KillInfo {
		val positionCode = KillMapProvider.getPositionCode(position)
		val entry = data[positionCode] ?: return KillInfo(0, 0, 0)

		val typeCode = KillMapProvider.getTypeCode(entityType)
		val typeKills = entry.getKills(typeCode)

		return KillInfo(typeCode, typeKills, entry.summaryKills)
	}

	fun addKill(position: Vec3d, entityType: EntityType<*>) {
		val positionCode = KillMapProvider.getPositionCode(position)
		val typeCode = KillMapProvider.getTypeCode(entityType)

		data.compute(positionCode) { _, entry ->
			(entry ?: RegionKillMap()).apply {
				addKills(typeCode)
			}
		}
	}
}