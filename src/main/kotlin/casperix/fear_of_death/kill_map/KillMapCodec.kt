package casperix.fear_of_death.kill_map

import net.minecraft.nbt.NbtCompound
import net.minecraft.util.math.Vec3i

object KillMapCodec {
	fun readNbt(nbt: NbtCompound, data: MutableMap<Vec3i, RegionKillMap>) {
		data.clear()

		val rawData = nbt.getIntArray("killMap")
		var offset = 0
		while (offset < rawData.size) {
			val positionCodeX = rawData[offset++]
			val positionCodeY = rawData[offset++]
			val positionCodeZ = rawData[offset++]
			val positionCode = Vec3i(positionCodeX, positionCodeY, positionCodeZ)

			val typeMap = RegionKillMap()
			data[positionCode] = typeMap

			val subMapSize = rawData[offset++]
			(0 until subMapSize).forEach {
				val typeCode = rawData[offset++]
				val typeAmount = rawData[offset++]
				typeMap.addKills(typeCode, typeAmount)
			}
		}
	}

	fun writeNbt(nbt: NbtCompound, data: MutableMap<Vec3i, RegionKillMap>) {
		val rawData = data.flatMap { (positionCode, entry) ->
			listOf(positionCode.x, positionCode.y, positionCode.z, entry.amountsByTypes.size) +
					entry.amountsByTypes.flatMap { (typeCode, typeAmount) ->
						listOf(typeCode, typeAmount)
					}
		}.toIntArray()

		nbt.putIntArray("killMap", rawData)
	}
}