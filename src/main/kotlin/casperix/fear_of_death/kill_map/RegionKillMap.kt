package casperix.fear_of_death.kill_map

class RegionKillMap() {
	var summaryKills = 0
	val amountsByTypes = mutableMapOf<Int, Int>()

	fun getKills(typeCode: Int): Int {
		return amountsByTypes.getOrDefault(typeCode, 0)
	}

	fun addKills(typeCode: Int, amount:Int = 1) {
		summaryKills += amount
		amountsByTypes.compute(typeCode) { _, lastAmount -> (lastAmount ?: 0) + amount }
	}
}