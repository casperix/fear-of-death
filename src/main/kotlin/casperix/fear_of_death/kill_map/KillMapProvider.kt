package casperix.fear_of_death.kill_map

import casperix.fear_of_death.FearOfDeathConfig
import net.minecraft.entity.EntityType
import net.minecraft.nbt.NbtCompound
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.math.Vec3d
import net.minecraft.util.math.Vec3i
import net.minecraft.world.PersistentState
import kotlin.math.roundToInt

class KillMapProvider : PersistentState() {
	private val globalMap = GlobalKillMap()

	override fun writeNbt(nbt: NbtCompound): NbtCompound {
		KillMapCodec.writeNbt(nbt, globalMap.data)
		return nbt
	}

	companion object {
		private var dirtyLevel = 0

		private fun readNbt(nbt: NbtCompound): KillMapProvider {
			val provider = KillMapProvider()
			KillMapCodec.readNbt(nbt, provider.globalMap.data)
			return provider
		}

		fun getTypeCode(entityType: EntityType<*>): Int {
			return entityType.translationKey.hashCode()
		}

		fun getPositionCode(pos: Vec3d): Vec3i {
			val range = FearOfDeathConfig.fearRange
			return Vec3i((pos.x / range).roundToInt(), (pos.y / range).roundToInt(), (pos.z / range).roundToInt())
		}

		fun getPositionCode(pos: Vec3i): Vec3i {
			val range = FearOfDeathConfig.fearRange
			return Vec3i(pos.x / range, pos.y / range, pos.z / range)
		}

		fun getMap(world: ServerWorld, readOnly:Boolean): GlobalKillMap {
			val provider = world.persistentStateManager.getOrCreate(
				Companion::readNbt,
				{ KillMapProvider() },
				"SPAWN_CONTROLLER_DEATH_MAP_STATE"
			)
			if (!readOnly) {
				dirtyLevel++
				if (dirtyLevel > FearOfDeathConfig.deathAmountForSave) {
					dirtyLevel = 0
					provider.markDirty()
				}
			}
			return provider.globalMap
		}
	}


}