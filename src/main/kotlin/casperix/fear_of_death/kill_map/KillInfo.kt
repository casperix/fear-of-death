package casperix.fear_of_death.kill_map

class KillInfo(val typeCode: Int, val concreteKills: Int, val summaryKills: Int)