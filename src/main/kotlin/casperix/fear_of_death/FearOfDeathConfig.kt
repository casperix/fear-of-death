package casperix.fear_of_death

object FearOfDeathConfig {
	const val fearRange = 32
	const val deathAmountForSave = 5
	const val onlyPlayerKillConsider = true

	const val concreteDeathLimit = 1
	const val summaryDeathLimit = 5
}