package casperix.fear_of_death.events

import casperix.fear_of_death.FearOfDeathConfig
import casperix.fear_of_death.kill_map.KillMapProvider
import net.minecraft.entity.EntityType
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.math.Vec3i


object SpawnEvent {

	@JvmStatic
	fun canSpawn(world: ServerWorld, entityType: EntityType<*>, position: Vec3i): Boolean {
		val map = KillMapProvider.getMap(world, true)
		val info = map.getKillInfo(position, entityType)
		if (info.concreteKills >= FearOfDeathConfig.concreteDeathLimit || info.summaryKills >= FearOfDeathConfig.summaryDeathLimit) {
			return false
		}

		return true
	}

}

