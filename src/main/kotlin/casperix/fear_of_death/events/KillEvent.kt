package casperix.fear_of_death.events

import casperix.fear_of_death.FearOfDeathConfig
import casperix.fear_of_death.kill_map.KillMapProvider
import net.fabricmc.fabric.api.entity.event.v1.ServerLivingEntityEvents
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.damage.DamageSource
import net.minecraft.server.world.ServerWorld

object KillEvent : ServerLivingEntityEvents.AllowDeath {
	override fun allowDeath(entity: LivingEntity, damageSource: DamageSource, damageAmount: Float): Boolean {
		if (!FearOfDeathConfig.onlyPlayerKillConsider || damageSource.source?.isPlayer == true) {
			playerKilledMonster(entity.world as ServerWorld, entity)
		}
		return true
	}

	private fun playerKilledMonster(world: ServerWorld, monster: LivingEntity) {
		val map = KillMapProvider.getMap(world, false)
		map.addKill(monster.pos, monster.type)
	}
}