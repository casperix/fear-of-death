/**
 * Created by casper on 02.06.2023.
 */
package casperix.mixin;

import casperix.fear_of_death.events.SpawnEvent;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnRestriction;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.SpawnHelper;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(SpawnHelper.class)
public abstract class SpawnHelperMixin {

	@Inject(at = @At("HEAD"), cancellable = true, method = "canSpawn(Lnet/minecraft/entity/SpawnRestriction$Location;Lnet/minecraft/world/WorldView;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/entity/EntityType;)Z")
	private static void init(SpawnRestriction.Location location, WorldView world, BlockPos pos, EntityType<?> entityType, CallbackInfoReturnable<Boolean> cir) {
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();

		if (world instanceof ServerWorld serverWorld) {
			if (!SpawnEvent.canSpawn(serverWorld, entityType, new Vec3i(x, y, z))) {
				cir.setReturnValue(false);
			}
		}
	}
}