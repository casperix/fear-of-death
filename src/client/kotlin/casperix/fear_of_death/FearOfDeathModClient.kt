package casperix.fear_of_death

import net.fabricmc.api.ClientModInitializer

object FearOfDeathModClient : ClientModInitializer {
	override fun onInitializeClient() {
		// This entrypoint is suitable for setting up client-specific logic, such as rendering.
	}
}